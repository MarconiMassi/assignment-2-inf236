// Parallel greedy matching
//
// n     : number of vertices
// ver   : pointers into the edges array to specify where each edge list starts and ends
// edges : edge lists for each vertex
// p     : array of length (at least) n. This should contain the matching partner of each vertex
// s     : helper array of length n
// t     : helper array of length n
//
#include "omp.h"


void pmatch(int n, int* ver, int* edges, int* p, int* s, int* t) {

	//MASSIMO: Helpful variables for the parallel algorithm
	int thread_ID, number_of_threads, i, j;
	thread_ID = omp_get_thread_num() + 1; //MASSIMO: add +1 in order to have the threads starting from 1 to n, instead of 0 to n-1
	number_of_threads = omp_get_num_threads();  //MASSIMO: Used to calculate the dimension fo the chuncks each thread needs to work on

	//MASSIMO: variables used to store information about the range of the vertex each thread need to work on
	long initial_vertex, last_vertex, offset, counter;

	//MASSIMO: this code region divides the vertices among all the threads
	offset = (long)(n / number_of_threads);
	initial_vertex = offset * (thread_ID - 1);
	if (thread_ID == number_of_threads) {
		last_vertex = n;
	}
	else {
		last_vertex = offset * thread_ID - 1;
	}

	//MASSIMO: set all the vertices with the closest neighbour (tested that more or less 30-35% of the nodes are set correctly)
	for (j = initial_vertex; j <= last_vertex; j++) {
		p[j] = edges[ver[j]];
	}
#pragma omp barrier

//#pragma omp master
//	{
//		printf("number of vertices wrongly matched before second parallel greedy matching: %li\n", correctedMatchCheck(s, ver, edges, n));
//	}

//	for (i = initial_vertex; i <= last_vertex; i++) {
//		if (s[s[i]] == i) {
//			p[i] = s[i];
//		}
//		else {
//			for (j = ver[i]; j < ver[i + 1]; j++) {
//				if (s[edges[j]] != s[s[edges[j]]]) {  //Is the if condition correct?
//					p[i] = edges[j];
//					p[edges[j]] = i;
//					break;
//				}
//			}
//		}
//	}
//#pragma omp barrier

//#pragma omp master
//	{
//		printf("number of vertices wrongly matched before sequential iteration: %li\n", correctedMatchCheck(p, ver, edges, n));
//	}

#pragma omp master
    {
		for (i = 1; i <= n; i++) {
			if (p[p[i]] != i) {
				p[i] = 0;
				for (j = ver[i]; j < ver[i + 1]; j++) {
					if (p[edges[j]] == 0) {
						p[i] = edges[j];
						p[edges[j]] = i;
						break;
					}
				}
			}
		}
    }

//#pragma omp master
//	{
//		printf("number of wrongly matched vertices is: %li\n", correctedMatchCheck(p, ver, edges, n));
//	}
}


int correctedMatchCheck(int *p, int* ver, int* edges, int n) {
	long counter = 0, i, j;

	for (i = 1; i <= n; i++) {
		if ((p[i] < 0) || (p[i] > n)) {
			counter++;
		}
		else if ((p[i] != 0) && (p[p[i]] != i)) {
			counter++;
		}
		else if (p[i] == 0) {
			for (j = ver[i]; j < ver[i + 1]; j++) {
				int v = edges[j];
				if (p[edges[j]] == 0) {
					counter++;
				}
			}
		}
	}

	return counter;
}